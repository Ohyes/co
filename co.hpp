#pragma once

#include "utils/log.hpp"

#include <list>
#include <functional>
#include <ucontext.h>

namespace Coroutine {
namespace this_co {
    void yield();
    void exit();
}

class Manager {
private:
    class CarrierBase {
    public:
        virtual void invoke() = 0;
        virtual ucontext_t& context() = 0;
        virtual void yield() = 0;
        virtual void exit() = 0;
        virtual ~CarrierBase() {}
    };

    template <typename F>
    class Carrier : public CarrierBase {
    private:
        ucontext_t cntx;
        std::function<F> func;
        Manager* host;

        static void func_wrapper(CarrierBase* p) {
            p->invoke();
        }

        void exit() {
            // remove it from ready list
            host->ready_list.pop_front();
            
            delete this;

            // return to sched, never return
            setcontext(&host->cntx);
        }

        void yield() {
            auto* co = host->ready_list.front();
            host->ready_list.pop_front();
            host->ready_list.push_back(co);

            swapcontext(&cntx, &host->cntx);
        }

    public:
        Carrier(Manager* host_, std::function<F> func)
            : func(std::move(func)), host(host_) {
            // allocate stack
            cntx.uc_stack.ss_sp = std::malloc(STACK_SIZE);
            cntx.uc_stack.ss_size = STACK_SIZE;

            // initialize context
            getcontext(&cntx);
            makecontext(&cntx, (void (*)(void))func_wrapper, 1, this);
        }

        ~Carrier() {
            std::free(cntx.uc_stack.ss_sp);
        }

        ucontext_t& context() {
            return cntx;
        }

        void invoke() {
            func();
            exit();
        }
    };

private:
    ucontext_t cntx;
    std::list<CarrierBase*> ready_list;

public:
    static const uint32_t STACK_SIZE = 1024 * sizeof(void*);

public:
    Manager() {
        cntx.uc_stack.ss_sp = std::malloc(STACK_SIZE);
        cntx.uc_stack.ss_size = STACK_SIZE;

        extern Manager* co_mgr;
        co_mgr = this;
    }

    ~Manager() {
        std::free(cntx.uc_stack.ss_sp);
    }

    template <typename F, typename... Args>
    void spawn(F&& f, Args&&... args) {
        using R = std::result_of_t<F(Args...)>;
        std::function<R(void)> func =
            std::bind(std::forward<F>(f), std::forward<Args>(args)...);

        // spawn a coroutine, and set it ready
        CarrierBase* co = new Carrier<R(void)>(this, func);
        ready_list.push_back(co);
    }

    void schedule() {
        while (!ready_list.empty()) {
            auto* co = ready_list.front();
            swapcontext(&cntx, &co->context());
        }
    }

    void try_yield_first() {
        if (!ready_list.empty()) {
            auto* co = ready_list.front();
            co->yield();
        }
    }

    void try_exit_first() {
        if (!ready_list.empty()) {
            auto* co = ready_list.front();
            co->exit();
        }
    }
};
}

