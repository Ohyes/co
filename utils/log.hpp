#pragma once

#include "gettid.hpp"
#include <sys/time.h>

#define LOG_FATAL "FATAL"
#define LOG_WARN "WARN"
#define LOG_INFO "INFO"
#define LOG_DEBUG "DEBUG"

#define LOG(level, fmt, args...)                                   \
    do {                                                                \
        struct timeval tm;                                              \
        gettimeofday(&tm, 0);                                           \
        fprintf(stderr, "[%s] [%s][%s][%ld] %d [%s][%d][%s] " fmt "\n", \
                LOG_##level, __DATE__, __TIME__, tm.tv_usec, gettid(),  \
                __FILE__, __LINE__, __PRETTY_FUNCTION__, ##args);       \
    } while (0)
