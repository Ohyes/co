#include "co.hpp"

namespace Coroutine {
Manager* co_mgr = nullptr;

namespace this_co {
    void yield() {
        co_mgr->try_yield_first();
    }

    void exit() {
        co_mgr->try_exit_first();
    }
}
}
