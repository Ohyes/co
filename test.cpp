#include "co.hpp"
#include <iostream>

void func(int x) {
    LOG(INFO, "func start");

    std::cout << x << std::endl;

    Coroutine::this_co::yield();

    LOG(INFO, "yield");

    Coroutine::this_co::exit();

    std::cout << x + 1 << std::endl;
}

int main() {
    Coroutine::Manager mgr;

    for (int i = 0; i < 3; ++i) {
        mgr.spawn(func, i);
    }
    mgr.schedule();

    return 0;
}
